package com.mpbruzina;

import com.mpbruzina.config.WebConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@SpringBootApplication
@EnableResourceServer
@ComponentScan(basePackages = {"com.mpbruzina"})
@Import({WebConfig.class})
public class ProxyServiceApplication extends ResourceServerConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(ProxyServiceApplication.class, args);
    }

    //filter only the resources we want protected with oauth2 security
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/mobile","/webapp*")
                .and()
                .authorizeRequests()
                .anyRequest().authenticated();
    }
}
