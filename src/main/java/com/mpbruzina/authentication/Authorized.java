package com.mpbruzina.authentication;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface Authorized {
    Clients[] clients();
}

