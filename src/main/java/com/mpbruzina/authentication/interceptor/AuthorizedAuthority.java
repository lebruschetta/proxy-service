package com.mpbruzina.authentication.interceptor;

import com.mpbruzina.authentication.Authorized;
import com.mpbruzina.authentication.Clients;
import com.mpbruzina.authentication.InvalidAuthorityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthorizedAuthority {

    Logger logger = LoggerFactory.getLogger(getClass());

    public void authorize(Authorized authorized) {
        Authentication authenticationFromRequest =SecurityContextHolder
                .getContext()
                .getAuthentication();
        logger.info("Attempting to authorize => {}", authenticationFromRequest);

        List<Clients> allowedAuthorities = Arrays.asList(authorized.clients());
        logger.info("Allowed: {}", allowedAuthorities);

        List<Clients> clientAuthorities = authenticationFromRequest
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .map(String::toUpperCase)
                .map(Clients::fromValue)
                .collect(Collectors.toList());
        logger.info("Authorities we have: {}", clientAuthorities);

        if (!allowedAuthorities.stream().anyMatch(clientAuthorities::contains)) {
            String url = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest().getRequestURI();
            logger.info("Failed authority authorization for url: {} and client: {}", url, clientAuthorities);
            throw new InvalidAuthorityException(url, clientAuthorities);
        }
        logger.info("Passed authority authorization");
    }
}
