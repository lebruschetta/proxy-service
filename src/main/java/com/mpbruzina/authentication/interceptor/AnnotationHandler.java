package com.mpbruzina.authentication.interceptor;

import com.mpbruzina.authentication.Authorized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Component
public class AnnotationHandler {
    Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    AuthorizedAuthority authorizedAuthority;

    public void handle(HandlerMethod handlerMethod, HttpServletRequest request) {
        if(hasAuthorizedMethodAnnotation(handlerMethod) && getAuthorities().size() != 0) {
            logger.info("Authorizing authority...");
            authorizedAuthority.authorize(handlerMethod.getMethodAnnotation(Authorized.class));
        }
    }

    private Collection<GrantedAuthority> getAuthorities(){
        return (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }

    private boolean hasAuthorizedMethodAnnotation(HandlerMethod handlerMethod) {
        Authorized apps = handlerMethod.getMethodAnnotation(Authorized.class);
        return (null != apps && apps.clients().length >= 1);
    }

}
