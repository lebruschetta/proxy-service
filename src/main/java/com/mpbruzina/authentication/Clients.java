package com.mpbruzina.authentication;


import java.util.HashMap;
import java.util.Map;


public enum Clients {
    SERVICE("ROLE_SERVICE"),
    WEBAPP("ROLE_WEBAPP"),
    MOBILE("ROLE_MOBILE");

    String clientId;

    Clients(String clientId){
        this.clientId = clientId;
    }

    private static Map<String, Clients> clientsMap = new HashMap<String,Clients>();

    static{
        for(Clients c: Clients.values()){
            clientsMap.put(c.getClientId(), c);
        }
    }

    public String getClientId() {
        return clientId;
    }

    public static Clients fromValue(String value) {
        return clientsMap.get(value);
    }
}
