package com.mpbruzina.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class InvalidAuthorityException extends RuntimeException {
    Logger logger = LoggerFactory.getLogger(getClass());

    String url;
    List<Clients> requestAuthority;

    public InvalidAuthorityException(String url, List<Clients> requestAuthority) {
        this.url = url;
        this.requestAuthority = requestAuthority;
    }

    @Override
    public String getMessage() {
        return "Unauthorized request to " + url + " => " +requestAuthority;
    }
}
