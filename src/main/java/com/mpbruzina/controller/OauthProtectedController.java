package com.mpbruzina.controller;

import com.mpbruzina.authentication.Authorized;
import com.mpbruzina.authentication.Clients;
import com.mpbruzina.authentication.InvalidAuthorityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class OauthProtectedController {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Authorized(clients = {Clients.MOBILE})
    @RequestMapping(value = "/mobile", method = RequestMethod.GET)
    String onlyAccessibleByMobile(){
        return "You must be a mobile client...";
    }

    @Authorized(clients = {Clients.WEBAPP})
    @RequestMapping(value = "/webapp", method = RequestMethod.GET)
    String onlyAccessibleByWebapp(){
        return "Definitely called from a webapp";
    }

    @Authorized(clients = {Clients.WEBAPP, Clients.MOBILE})
    @RequestMapping(value = "/webappOrMobile", method = RequestMethod.GET)
    String accessibleByWebappOrMobile(){
        return "Not sure what you are, but I'll trust you";
    }

    @RequestMapping(value = "/unprotected", method = RequestMethod.GET)
    String lolTedNugentForPresident(){
        return "It's a free-for-all";
    }

    @ExceptionHandler(InvalidAuthorityException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void handleUnauthorizedRequests(InvalidAuthorityException e){
        logger.info("User request not authorized: {}", e.getMessage());
    }

}
