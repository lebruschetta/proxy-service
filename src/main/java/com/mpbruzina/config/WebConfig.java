package com.mpbruzina.config;

import com.mpbruzina.authentication.interceptor.AuthorizationRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
    /*
    Wire in the AuthorizationInterceptor to secure endpoints to only the authorized clients
     */

    @Bean
    AuthorizationRequestInterceptor authorizationRequestInterceptor() {
        return new AuthorizationRequestInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authorizationRequestInterceptor());
        super.addInterceptors(registry);
    }

}
